Rédaction informatique et Mathématique L1 S1
Audrey Martin, Mohamed Sadane , Tom Duconge. Portail Mathématique Informatique MI5.

Projet LaTeX et git: rapport d'exposé sur le Triangle de Pascal.

Nous devons réaliser un rapport sur le triangle de Pascal, en travaillant en équipe de 3 personnes. Nous utiliserons LaTeX et 
git, avec un projet hébergé sur gitlab. Le rapport traitera des  différents aspects englobant le triangle de Pascal, incluant 
formules, théorèmes avec preuves, et exemples. Nous devons suivre des normes LaTeX spécifiques et des conventions de codage. 
L'avancée de nos travaux sur ce projet sera partagée via git . Le rapport final et un fichier texte associé  seront soumis en ligne sur le gitlab du projet.
