\documentclass[titlepage]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{array}
\usepackage[french]{babel}
\newcommand{\nk}{\binom{n}{k}}

\title{Le Triangle de Pascal}
\author{Audrey Martin, Mohamed Saidane, Tom Duconge (TheBlackHole)}
\date{14 Janvier 2024}

\begin{document}

\maketitle

\tableofcontents
\newpage

\section{Introduction}
Le Triangle de Pascal est un arrangement triangulaire de nombres qui a captivé les mathématiciens depuis des siècles. Cette structure révèle des relations et des propriétés mathématiques profondes, trouvant des applications dans divers domaines comme l'algèbre, la combinatoire et la théorie des probabilités.

\subsection{Présentation générale du Triangle de Pascal}
Le Triangle de Pascal, nommé d'après le mathématicien français Blaise Pascal, est connu dans le monde occidental sous ce nom, bien que sa découverte remonte à des mathématiciens bien plus anciens en Inde, en Perse, et en Chine. Par exemple, il était connu sous le nom de "triangle de Khayyam" en Perse, et de "triangle de Yang Hui" en Chine.

Historiquement, le triangle était utilisé pour présenter les coefficients binomiaux, qui sont disposés dans un tableau triangulaire. La construction du triangle suit la relation de Pascal, qui stipule que chaque élément du triangle est la somme des deux éléments situés juste au-dessus de lui. La première ligne du triangle est composée d'un seul 1, et chaque ligne supplémentaire se développe en ajoutant des 1 sur les côtés et en calculant les valeurs internes comme la somme des deux valeurs supérieures.

Le Triangle de Pascal peut être généralisé à d'autres dimensions, formant des structures telles que la pyramide de Pascal en trois dimensions. Ses propriétés et applications sont vastes, allant de la simple présentation des coefficients binomiaux à des applications en théorie des probabilités et au-delà.


\section{Fondements Mathématiques}
\subsection{Ensemble des nombres entiers}
En mathématiques, un nombre naturel noté $\mathbb{N}$ représente essentiellement une unité de compte permettant de compter des objets équivalents, comme des jetons (un, deux...) ou des cartes (un, deux, trois...). Ces nombres peuvent être exprimés en notation décimale positionnelle, sans signes ni virgules, en utilisant une séquence finie de chiffres.

L'arithmétique, une branche des mathématiques qui existe depuis la Grèce antique, traite de l'étude approfondie des nombres naturels. Chaque nombre naturel a un successeur unique, le nombre immédiatement supérieur, et la série des nombres naturels est infinie.
\vspace{30pt}

\subsection{Définition et notation d'une factorielle}
La factorielle d'un nombre naturel n est le produit de tous les nombres naturels inférieurs ou égaux à n. Il est noté $n!$, soit $n! = 1 * 2 * 3 * 4 * ... * n$. Exemple : $7 ! = 1 * 2 * 3 * 4 * 5 * 6 * 7 = 5,040$. Dans cette formule, le nombre 7 est appelé « factoriel de 7 », et pour l'obtenir il faut multiplier tous les nombres qui apparaissent dans la formule jusqu'à atteindre 1.

\subsection{Définition et notation du nombre de combinaisons}
Les combinaisons, souvent appelées $C(n,k)$ ou $\nk$, représentent le nombre de façons différentes de choisir $k$ éléments parmi un ensemble de $n$ éléments, quel que soit l'ordre. Dans le triangle de Pascal, ces coefficients binomiaux créent une structure triangulaire dans laquelle chaque nombre est la somme des deux nombres directement au-dessus de lui. La notation $C(n,k)$ peut être interprétée comme le nombre de combinaisons possibles de $k$ éléments sélectionnés parmi $n$ éléments.

\section{Valeurs Triviales et Exemples}
\subsection{Valeurs triviales de combinaisons}

Soit n un nombre naturel différent de zéro et $k$ un nombre naturel tel que $1 \leq k \leq n$. Le nombre $\nk$ de sous-ensembles de taille $k$ d'un ensemble de $n$ éléments est le nombre de façons de choisir $k$ éléments parmi $n$ éléments. En utilisant des factorielles, on peut exprimer $\nk$ de la manière suivante : $\nk =\frac{ n!}{(k!(n-k!)}$ Cette expression peut être interprétée comme le nombre de façons de choisir $k$ éléments parmi n éléments, en tenant compte du nombre de façons d'arranger les $k$ éléments choisis. Exemples:
Pour $n = 3$ et $k = 2$, on a $\binom{3}{2} = \frac{3 !}{(2!(3-2)!)} = 3$. Il y a donc 3 façons de choisir 2 éléments parmi 3 éléments.
Pour $n = 4$ et $k = 3$, on a $\binom{4}{3} = \frac{4 !}{(3!(4-3)!)} = 4$. Il y a donc 4 façons de choisir 3 éléments parmi 4 éléments.\\

Quelques valeurs triviales:
\begin{itemize}
    \item lorsque $k = 0$, $\binom{n}{0} = \frac{n!}{(0!(n-0)!)} = \frac{n!}{n!} = 1$
    \item lorsque $k = 1$, $\binom{n}{1} = \frac{n!}{(1!(n-1)!)} = \frac{n!}{(n-1)!} = n$
    \item lorsque $k = n$, $\binom{n}{n} = \frac{n!}{(n!(n-n)!) }= \frac{n!}{n!} = 1$
    \item lorsque $k = n-1$, $\binom{n}{n-1}= \frac{n!}{((n-1)!(n-n+1)!)} = \frac{n!}{(n-1)!} = n$
\end{itemize}

\begin{figure}
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{fac.PNG}
    \caption{courbe de $n!$}
  \end{minipage}
  \begin{minipage}{0.5\textwidth}
    \centering
    \includegraphics[width=0.8\textwidth]{nk.PNG}
    \caption{courbe de $\binom{n}{k}$}
  \end{minipage}
\end{figure}

\section{Triangle de Pascal}
\subsection{Exemples de Triangle de Pascal}
Soit $A$ un ensemble de $k$ éléments et $B\subseteq A$ possède $i$ éléments.
\\

\begin{tabular}{>{$n=$}l<{\hspace{12pt}}*{13}{c}}
0 ~~~~~~~~~~~~~~~~~1~~~~~~~\\
1 ~~~~~~~~~~~~~~~1~~~1~~~~~~\\
2 ~~~~~~~~~~~~~1~~~2~~~1~~~~~\\
3 ~~~~~~~~~~~1~~~3~~~3~~~1~~~~\\
4 ~~~~~~~~~1~~~4~~~6~~~4~~~1~~~\\
5 ~~~~~~1~~~5~~~10~~~10~~~5~~~1~~\\
6 ~~~~1~~~6~~~15~~~20~~~15~~~6~~~1~\\
7 ~~1~~~7~~~21~~~35~~~35~~~21~~~7~~~1
\end{tabular}
\subsection{Théorème et preuve}
Ce théorème s'appelle la relation de Pascal. Cette relation est la suivante : pour tous les nombres naturels $n$ et $k$ tels que $0 \leq k \leq n$, elle indique que le coefficient binomial $\nk$ est égal à la somme des coefficients binomiaux adjacents sur la ligne précédente du Triangle de Pascal,  c'est-à-dire $(n-1 k) + (n-1 k-1)$. Cette égalité s'exprime formellement par $\nk = (n-1 k) + (n-1 k-1)$.

\section{Calcul et Algorithmes}
\subsection{Algorithme de calcul des valeurs de combinaisons}

Pour calculer efficacement les coefficients binomiaux \(\nk\) pour tous \(k\) et \(n\) tels que \(0 < k \leq n\), l'algorithme suivant est basé sur une approche dynamique, exploitant la propriété récursive des relations de Pascal :

\begin{enumerate}
    \item \textbf{Initialisation} : Créer une matrice \(C\) de dimensions \((n+1) \times (k+1)\) pour stocker les coefficients binomiaux.
    \item \textbf{Initialisation du cas de base} : Remplir la première colonne de \(C\) avec des 1, car \(\binom{n}{0} = 1\) pour tout \(n\), et remplir la diagonale principale avec des 1, car \(\binom{n}{n} = 1\) pour tout \(n\).
    \item \textbf{Calculs itératifs} : Utiliser la relation de Pascal \(\nk = \binom{n-1}{k} + \binom{n-1}{k-1}\) pour remplir de manière itérative la matrice \(C\), en commençant par \(k = 1\) jusqu'à \(k = n\) et pour \(n\) allant de 1 jusqu'à la fin.
    \item En termes mathématiques, cela s'écrit comme : 
    \[ C[n][k] = C[n-1][k-1] + C[n-1][k] \]
    \item \textbf{Résultat} : Une fois l'algorithme entièrement exécuté, la matrice \(C\) contient tous les coefficients binomiaux souhaités.
\end{enumerate}

\section{Théorèmes et Preuves}
\subsection{Théorème de binôme de Newton et sa preuve}


Le théorème du binôme, un résultat fondamental en algèbre, est formulé comme suit :

Pour tout entier positif \( n \), l'expression \( (a + b)^n \) peut être développée en une somme de termes selon la formule :

\[
(a + b)^n = \sum_{k=0}^{n} \nk a^k b^{n-k}
\]

Ce qui indique que l'expression \( (a + b)^n \) est équivalente à une somme de termes, où chaque terme est le produit d'un coefficient binomial \( \nk \) avec \( a^k \) et \( b^{n-k} \). Cette formule liste toutes les manières possibles de sélectionner \( k \) occurrences de \( a \) et \( n-k \) occurrences de \( b \) dans le développement de la puissance \( n \).

\section{Ressources Bibliographiques}
\begin{itemize}
    \item \href{https://www.youtube.com/watch?v=JwMXMl3oaa0&ab_channel=jaicomprisMaths}{Formule du triangle de Pascal, démonstration par le calcul (Vidéo)}
    \item \href{https://www.youtube.com/watch?v=raWJysLaIsU&ab_channel=jaicomprisMaths}{Démonstration de la formule du binôme de Newton (Vidéo)}
    \item \href{https://fr.wikipedia.org/wiki/Entier_naturel}{Entier Naturel - Wikipédia}
    \item \href{https://fr.wikipedia.org/wiki/Triangle_de_Pascal}{Triangle de Pascal - Wikipédia}
\end{itemize}

\section{Conclusion}
À travers ce projet, nous avons exploré les divers aspects du Triangle de Pascal, depuis ses fondements mathématiques jusqu'à ses applications concrètes. L'étude de l'ensemble des nombres entiers, des factorielles et des combinaisons nous a permis de mieux comprendre la structure et la signification du triangle.

Les sections traitant des valeurs triviales et des exemples ont offert des illustrations pratiques de la théorie, rendant le sujet plus accessible. L'analyse des exemples du Triangle de Pascal et la preuve du théorème de la relation de Pascal ont ajouté une dimension concrète à notre compréhension.

La section sur les algorithmes de calcul a montré comment les principes du Triangle de Pascal peuvent être appliqués en informatique, démontrant son utilité dans le monde numérique.

En conclusion, ce projet sur le Triangle de Pascal a été une opportunité d'approfondir nos connaissances en mathématiques et de découvrir comment des concepts théoriques peuvent trouver des applications pratiques.


\end{document}